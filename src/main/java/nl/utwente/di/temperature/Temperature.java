package nl.utwente.di.temperature;

import java.io.*;
import jakarta.servlet.*;
import jakarta.servlet.http.*;

/** Example of a Servlet that gets an ISBN number and returns the book price
 */

public class Temperature extends HttpServlet {
 	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

  public void doGet(HttpServletRequest request,
                    HttpServletResponse response)
      throws ServletException, IOException {

    response.setContentType("text/html");
    PrintWriter out = response.getWriter();
    String title = "Temperature in Fahrenheit";
    
    // Done with string concatenation only for the demo
    // Not expected to be done like this in the project
    out.println("<!DOCTYPE HTML>\n" +
                "<HTML>\n" +
                "<HEAD><TITLE>" + title + "</TITLE>" +
                "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
                		"</HEAD>\n" +
                "<BODY BGCOLOR=\"#FDF5E6\">\n" +
                "<H1>" + title + "</H1>\n" +              
                "  <P>Degree in Celsius: " +
                   request.getParameter("temp") + "\n" +
                "  <P>Degree in Fahrenheit: " +
                        (((Integer.parseInt(request.getParameter("temp"))) * 1.8) + 32) +
                "<h1>Celsius to Fahrenheit Calculator Application</h1>\n" + "\n" + "<form ACTION=\"./temperature\">\n"
                        + "  <p>Enter Degree in Celsius:  <input TYPE=\"TEXT\" NAME=\"temp\"></p>\n"
                        + "  <input TYPE=\"SUBMIT\">\n" + "</form></BODY></HTML>");
  }
}
